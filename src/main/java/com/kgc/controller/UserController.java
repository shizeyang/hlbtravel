package com.kgc.controller;

import com.kgc.entity.User;
import com.kgc.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
@RequestMapping("user")
@ResponseBody
public class UserController {
    @Autowired
    private UserService userService;

    @RequestMapping("register")
    public  boolean testRegister(User user){
        return userService.registerUser(user)==1?true:false;
    }


    @RequestMapping("login")
    public int testLogin(String username, String password, String validationCode, HttpServletRequest request){
       User user = userService.userLogin(username,password);
        HttpSession session = request.getSession();
        String randomCode =  (String) session.getAttribute("randomCode");
        if (user == null){
           return 1;
        }else if (!randomCode.equals(validationCode)) {
            return 2;
        }else {
            int uid =  user.getId();

            session.setAttribute("uid",uid);

            session.setAttribute("user",user);

            return 3;
        }

    }
    @RequestMapping("list")
    public List<User> getUserList(int id){
        return userService.getUserList(id);
    }

    @RequestMapping("update")
    public boolean update(User user){
        return userService.update(user);
    }

    @RequestMapping("tologout")
    public String tologout(HttpServletRequest request){
        HttpSession session = request.getSession();
        session.invalidate();
        return "index";
    }


}
