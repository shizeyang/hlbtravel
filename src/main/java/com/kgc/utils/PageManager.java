package com.kgc.utils;

public class PageManager {
    //当前页数
    private  int pageNo;

    //总条数
    private int pageTotal;

    //煤业显示的条数
    private int pageSize;

    //总页数
    private int pageSizeTotal;

    public int getPageNo() {
        return pageNo;
    }

    public void setPageNo(int pageNo) {
        if (pageNo>0) {
            this.pageNo = pageNo;
        }
    }

    public int getPageTotal() {
        return pageTotal;
    }

    public void setPageTotal(int pageTotal) {
        if (pageTotal>0) {
            this.pageTotal = pageTotal;
            if (pageTotal%pageSize==0){
                setPageSizeTotal(pageTotal/pageSize);
            }else {
                setPageSizeTotal(pageTotal/pageSize+1);
            }

        }
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
       if (pageSize>0) {
           this.pageSize = pageSize;
       }
    }

    public int getPageSizeTotal() {
        return pageSizeTotal;
    }

    public void setPageSizeTotal(int pageSizeTotal) {
       if (pageSizeTotal>0) {
           this.pageSizeTotal = pageSizeTotal;
       }
    }
}
