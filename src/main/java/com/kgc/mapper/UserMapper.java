package com.kgc.mapper;

import com.kgc.entity.User;
import org.apache.ibatis.annotations.Param;

import java.util.List;
public interface UserMapper {
    //用户注册，增加用户
    int registerUser(@Param("user") User user);

    //用户登录
    User userLogin(@Param("username") String username,@Param("password") String password);

    List<User> getUserList(@Param("id") int id);

    public int update(@Param("user") User user);

}
