package com.kgc.service;

import com.kgc.entity.User;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UserService {
    int registerUser(User user);

    User userLogin(String username,String password);

    List<User> getUserList(int id);

    public boolean update(User user);

}
