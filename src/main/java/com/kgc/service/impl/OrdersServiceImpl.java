package com.kgc.service.impl;

import com.kgc.entity.Orders;
import com.kgc.mapper.OrdersMapper;
import com.kgc.service.OrdersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OrdersServiceImpl implements OrdersService {

    @Autowired
    private OrdersMapper ordersMapper;


    @Override
    public int addOrders(Orders orders) {
        return ordersMapper.addOrders(orders);
    }

    @Override
    public List<Orders> getOrdersList(int uid) {
        return ordersMapper.getOrdersList(uid);
    }

    @Override
    public int deleteOrders(String id) {
        return ordersMapper.deleteOrders(id);
    }
}
