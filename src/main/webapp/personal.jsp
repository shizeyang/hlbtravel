<%--
  Created by IntelliJ IDEA.
  User: 19245
  Date: 2019/11/4
  Time: 19:01
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="UTF-8">
    <title></title>
    <link rel="stylesheet" href="css/personal.css" />
    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/common.css">
    <link rel="stylesheet" type="text/css" href="css/index.css">
    <link rel="stylesheet" type="text/css" href="css/Order.css" />
</head>

<body>
<!--引入头部-->
<div id="header"></div>

<!--个人资料-->
<div style="width:100% ; height: 550px;">
    <ul class="sort">
        <li>
            <div style="height: 18px;"></div>
            <div class="navigation" >
                <br />
                <a href="personal.jsp">个人信息</a><hr style="height:1px;border:none;border-top:2px dotted #000000;"/>
                <a href="orders.jsp">我的订单</a><hr style="height:1px;border:none;border-top:2px dotted #000000;"/>
                <a href="myfavorite.jsp">我的收藏</a>

            </div>
        </li>
        <li>
            <div style="height: 18px;"></div>
            <div class="data_box">
                <div style="height: 25px;"></div>
                <div class="show_data" style="text-align: center">
                <div class="tbody" style="text-align: center;font-size: 20px;line-height: 30px;">

                    <!--插入个人信息-->
                </div>
                    <a href="#" style="color: #0000ff;">[修改个人信息]</a>

                </div>
            </div>
            <div style=" height: 18px;"></div>
        </li>
    </ul>
</div>
<!--导入底部-->
<div id="footer"></div>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="js/jquery-3.3.1.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/bootstrap.min.js"></script>
<!--导入布局js，共享header和footer-->
<script type="text/javascript" src="js/include.js"></script>
<script>
    $(function(){

        $.getJSON("user/list",{"id":"${sessionScope.user.id}"},function(data){

            if (data != null){
                var str = "";
                $(data).each(function(){

                    str +=
                        // "<tr>"+
                        "<tr><td>用户ID："+this.id+"</td></tr>"+
                        "<tr><td>用户名："+this.username+"</td></tr>"+
                        "<tr><td>Email："+this.email+"</td></tr>"+
                        "<tr><td>真实姓名："+this.name+"</td></tr>"+
                        "<tr><td>电话号码："+this.telephone+"</td></tr>"+
                        "<tr><td>性别："+this.sex+"</td></tr>"+
                        "<tr><td>生日："+this.birthday+"</td></tr>"
                        // "</tr>";
                })
                $(".tbody").append(str);
            }
        })
    })
</script>

</body>
</html>
