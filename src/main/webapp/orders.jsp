<html>
<head>
    <base href="<%=basePath%>" >

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>旅游网</title>
    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/common.css">
    <link rel="stylesheet" type="text/css" href="css/index.css">
    <link rel="stylesheet" type="text/css" href="css/Order.css">
    <link rel="stylesheet" type="text/css" href="css/personal.css">

    <style type="text/css">
 span{
         color: blue;
      }
span:hover{
    color: red;
    cursor: pointer;
}

.slider_rbanner {
    width: 70px;
    position: fixed;
    background-color: #fff;
	box-shadow: 0 2px 6px 0
    rgba(207, 206, 206, 0.5);
    border-radius: 4px;
    top: 50%;
    margin-top: -140px;
    right: 20px;
    z-index: 5;

}
ol, ul, iframe {
    margin: 0;
    margin-top: 0px;
}
.dNone {
    display: none;
}
.slider_rbanner li.kefu {
    background-color: #FFC900;
	color: #fff;
	margin-left:0 ;
}
.slider_rbanner li {
    height: 70px;
    margin-left:0 ;
    width: 100%;
    position: relative;
    margin-right: 100px;
}
.slider_rbanner span：hover {
	background-color:#FFC900;
}

.slider_rbanner li.kefu span {
    border-bottom: 1px solid orange;
}
.slider_rbanner li span {
	padding-top: 46px;
	border-bottom: 1px solid #f2f2f2;
    padding-top: 46px;
    margin: 0 5px;
    width: 58px;
    }
    .slider_rbanner li span {
    display: inline-block;
    height: 100%;
    text-align: center;
    font-size: 12px;
    letter-spacing: -1.6px;
}
.kefu span {
    background: url(images/kefu.png) no-repeat center 10px;
}
.tel span {
    background: url(images/ph.png) no-repeat center 10px;
}
.app span {
    background: url(images/app.png) no-repeat center 10px;
}
.gongzhonghao span {
    background: url(images/gongzhonghao.png) no-repeat center 10px;
}
.weixin span{
	background: url(images/xiao.png) no-repeat center 10px;
}
span:link, span:visited {
    color: inherit;
}
span:link, span:visited {
    color: inherit;
}
span {
	color: black;
    text-decoration: none;
    outline: none;
    cursor: pointer;
}
p{
    margin: 0;
    word-break: break-all;
}
.kefu div img {
    width: 185px;
    height: 35px;
    }
    </style>

</head>

<body>
<!--引入头部-->
<div id="header"></div>
<meta charset="UTF-8">

<!--订单-->
<div class="ab"    style="width:100% ; height: 900px;background: url(images/zhuo.jpg)no-repeat center;background-size: 100% 900px;">
    <ul class="sort">
        <li>
            <div style="height: 18px;"></div>
            <div class="navigation" style="box-shadow: 0px 0px 20px darkgrey;">
                <br />
                <div class="perinfo">
                    <div id="uploadify-button" class="uploadify-button " style="height: 80px; line-height: 80px; width: 80px;">
                    <img  src="images/huluobo.png" style="width: 80px;height: 80px;margin-bottom:  5px;border-radius: 40px;margin-left: 33px;">
                </div>
                   <a><img  src="images/shou1.png" style="width: 30px;height:30px;margin-bottom: 15px;border-radius: 40px;margin:auto;margin-top: 25px;"/></a>
                <button id="menuA" href="#" onclick="changeA()">个人信息</button>
                <button id="menuB" href="#" onclick="changeB()">我的订单</button>
                <button id="menuC" href="#" onclick="changeC()">我的收藏</button>

            </div>
        </li>
        <li>
            <div id="root" class="root-container">
                <ul id="base_wrapper">
                    <li id="base_bd" class="layout-nm">
                        <div id="leftNavWrapper" menutype="0" menuid="order_all">
                            <div id="sideNavCss">
                            </div>
                            <div id="sideNav" class="aside clearfix">
                                <div class="main">
                                    <div class="main-w">

                                        <div style="height: 18px;"></div>
                                        <div class="order_list_empty" style="box-shadow: 0px 0px 20px darkgrey;background-color: white;">
                                            <div class="nmsl">我的订单</div>
                                            <table style="width: 1000px;">
                                                <thead>
                                                <div>
                                                <tr class="nmsl" style="line-height: 50px;">
                                                    <th style="text-align: center;">订单编号</th>
                                                    <th style="text-align: center;">订单时间</th>
                                                    <th style="text-align: center;">订单详情</th>
                                                    <th style="text-align: center;">订单价格</th>
                                                    <th style="text-align: center;">订单状态</th>
                                                    <th style="text-align: center;">订单操作</th>
                                                </tr>
                                                </div>
								
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div style="height: 18px;"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <li>
                </ul>
            </div>
        </li>
        <ul class="slider_rbanner">

        <li class="kefu">
            <span>小助手</span>
        </li>
        <li class="tel" >
            <span>电话</span>
            <div class="dNone"><p>客服电话</p><p style="margin-top: 5px; font-weight:bold">400-618-9090</p></div>
        </li>
        <li class="app">
            <span>APP</span>
            <div class="dNone"><img src="images/shou.png" alt=""><p>扫码下载APP</p></div>
        </li>
        <li class="gongzhonghao">
            <span>公众号</span>
            <div class="dNone"><img src="images/shou.png" alt=""><img src="" alt=""><label>旅游</label><label>胡萝卜旅游网</label><p>扫一扫，关注公众号</p></div>
        </li>
        <li class="weixin">
            <span>小程序</span>
            <div class="dNone"><img src="images/shou.png" alt=""><p>扫码使用小程序</p></div>
        </li>
    </ul>
    </ul>
    
</div>
<!--导入底部-->
<div id="footer"></div>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="js/jquery-3.3.1.js"></script>
<script>
    $(function () {

        $.getJSON("orders/list",{"uid":${uid}},function (data) {

           if (data != null){
               var str = "";
               $(data).each(function(){
                   var statement = "已付款";
                   if (this.state==1){
                        statement = "未付款";
                    }
                   str +="<tr style='line-height: 100px;'>"+
                       "<td style=\"text-align: center;\">"+this.id+"</td>"+
                       "<td style=\"text-align: center;\">"+this.publishdate+"</td>"+
                       "<td style=\"text-align: center;\">"+this.details+"</td>"+
                       "<td style=\"text-align: center;\">"+this.price+"元</td>"+
                       "<td style=\"text-align: center;\">"+statement+"</td>"+
                       "<td align='center'><span onclick='del("+this.id+", this)'><img src='images/delete.png' ></span></td>"+
                       "</tr>";
               })
               $("tbody").append(str);
           }
        })
    })

    function del(id,obj) {

        if (confirm("确认删除吗？")){

          $.getJSON("orders/delete",{"id":id},function (data) {
                  if (data == 1) {
                      $(obj).parents("tr").remove();
                  }else {
                      alert("删除失败！")
                  }
          })

        }
    }
	
 $(window).scroll(function () {
                var theight = $(document).scrollTop();
                if (theight > 100) {
                    $(".fixed_buybox").removeClass("dNone");
                } else {
                    $(".fixed_buybox").addClass("dNone");
                }
            });

//$(function(){
//			$(".tel span").mouseover(function(){
//				var index = $(".slider_rbanner li span").index(this);
//				$(".slider_rbanner li:eq(" + index + ") span~div").show();
//			})
//			$(".slider_rbanner li span").mouseout(function(){
//				var inSdex = $(".slider_rbanner li span").index(this);
//				$(".slider_rbanner li:eq(" + index + ")span~div").hide();
//			})
//		})
</script>


<script src="js/bootstrap.min.js"></script>
<!--导入布局js，共享header和footer-->
<script type="text/javascript" src="js/include.js"></script>

<script type="text/javascript">
    function changeA() {

        document.getElementById("menuA").style.background = '#FFD800';

        document.getElementById("menuB").style.background = '#FFC900';

        document.getElementById("menuC").style.background = '#FFC900';

    }

    function changeB() {

        document.getElementById("menuB").style.background = '#FFD800';

        document.getElementById("menuA").style.background = '#FFC900';

        document.getElementById("menuC").style.background = '#FFC900';

    }

    function changeC() {

        document.getElementById("menuC").style.background = '#FFD800';

        document.getElementById("menuA").style.background = '#FFC900';

        document.getElementById("menuB").style.background = '#FFC900';
    }
</script>
</body>
</html>
