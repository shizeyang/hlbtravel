<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<!DOCTYPE html >
<head>
    <script src="../js/jquery-1.11.3.min.js" type="text/javascript"></script>
    <script src="../js/global.js?v=1.2.1" type="text/javascript"></script>
    <script src="js/Browser.js"></script>
	<link rel="icon" href="../images/bitbug_favicon.ico" type="image/x-icon" />
    <style type="text/css">
        body > div:first-child {
            z-index: 100 !important;
        }

        .kefu div {
            position: absolute;
            right: 56px;
            width: 185px;
            top: 0px;
            height: 35px;
            border-radius: 4px;
            padding: 10px;
            right: 68px;
        }

            .kefu div img {
                width: 185px;
                height: 35px;
            }

        .kefu:hover div {
            display: block;
        }

        .ieDiv {
            width: 400px;
            height: 32px;
            border-radius: 4px;
            background-color: #fffcf4;
            box-shadow: 0 2px 5px 0 #f8f2da;
            border: solid 1px #f8f6e8;
            float: left;
            margin-left:500px;
            padding-top: 5px;
            text-align: center;
            display:none;
        }

            .ieDiv > span {
                width: 304px;
                height: 14px;
                font-family: PingFangSC;
                font-size: 14px;
                font-weight: normal;
                font-style: normal;
                font-stretch: normal;
                line-height: 1.0;
                letter-spacing: normal;
                text-align: center;
                color: #ceac61;
            }
			.mystudy{ background-image: url("images/wodexuexi.png");}
.mystudy.cur{ background-image: url("images/wodexuexi01.png");}

    </style>
    
<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="chrome=1,IE=edge" />
    <title>个人中心-我的账户</title>
    <script type="text/javascript" src="../js/jquery-1.11.3.min.js"></script>
    <link rel="stylesheet" href="../css/main.css?v=2.2.1" />
    <link rel="stylesheet" href="../css/select2.css?v=1.2.1" />
    <link href="../css/newver.css" rel="stylesheet" type="text/css" />
    <link href="../css/newver.css" rel="stylesheet" />
    <script type="text/javascript" src="../js/select2.js"></script>
    <script type="text/javascript" src="../js/common.js?v=1.2.1"></script>
    <script type="text/javascript" src="../js/jquery.cityselect.js"></script>
    <script type="text/javascript" src="../js/My97DatePicker/WdatePicker.js"></script>
    <script src="../js/global.js?v=1.2.1" type="text/javascript"></script>
    <script src="../js/UserInfo/personalCenter.js?v=1.2.1" type="text/javascript"></script>
    <script src="../js/uploadFile/jquery.uploadify.js" type="text/javascript"></script>
    <script src="../js/jquery.pagination.js" type="text/javascript"></script>
    <style type="text/css">
    .uploadify{ position:absolute; left:70px; top:63px;}
    </style>
<title>

</title></head>
<body>
	
    <form method="post" action="MyAccount.aspx" id="form1" onsubmit="return false;">
<div class="aspNetHidden">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="/wEPDwUJNDMyNDU0NjAzZGRX+b9mUM8omrvJSwaf66A916I/sVZK7dTf/4W1Eti0Og==" />
</div>

        <div class="ieDiv">
            <span class="">您当前的IE浏览器版本过低建议您使用360浏览器急速模式打开</span>
        </div>
        <!-- 头部-->
        <div class="header">
            <div class="inwrapper">
                <span class="lglogo" onclick="window.location.href = '../main/main.aspx';"></span><span class="lglogan" onclick="window.location.href = '../main/main.aspx';"></span>
                <div class="header_nav">
                    <a class="cur" id="d0" onclick="changeSelect(this)" href="../BuyPackage/findMain.aspx">发现</a><a id="d1" onclick="gotoLearn(this,0)">学习中心</a><a href="https://wyk8.ke.qq.com/#tab=0&category=-1"
                            target="_blank" onclick="changeSelect(this)">直播</a><a href="http://www.wyk8.com/appdown.html" target="_blank"
                        id="d2" onclick="changeSelect(this)" class="app">下载</a>
                    <div class="lgre_ipwrapper serch_head">
                        <i onclick="searchInfo()"></i>
                        <input type="text" placeholder="请输入您要搜索的关键字" id="condition" />
                    </div>
                    <a class="news" onclick="gotoMessage()">消息<em id="n">0</em></a>
                    <div class="ucenter">
                        <span id="nickName"><em>hi！好好学习<img id="uImg" src="../images/zhanhu_toux_icon.png"></em></span>
                        <ul class="ucenter_list dNone">
                            <li class="mystudy"><a onclick="gotoLearn()">我的学习</a></li>
                            <li class="perset"><a href="../PersonalCenter/PersonalSet.aspx">个人设置</a></li>
                            <li class="perstudy"><a href="../PersonalCenter/StudyArchives.aspx">学习档案</a></li>
                            <li class="peraccout"><a href="../PersonalCenter/MyAccount.aspx">我的账户</a></li>
                            <li class="perorder"><a href="../PersonalCenter/MyOrder.aspx">我的订单</a></li>
                            <li class="perQuan"><a href="../PersonalCenter/MyCoupon.aspx" class="marlt20">我的优惠券</a></li>
                            <li class="perlgout"><a onclick="loginOut()" href="../Login.aspx">退出登录</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div>
            
<div class="mainbox">
        <div class="inwrapper  clearfloat">
            <!--左侧栏-->
            <div class="perleftbox">
                <div class="perinfo">
                    <input type="file" name="uploadify" id="uploadify" style="display:none"  />
                    <a ><img id="userImg" src="../images/usercent_user_icon.png"></a>
                    <div class="permoney"><i></i></div>
                </div>
                <ul class="ucenter_list">
                    <li class="mystudy"><a onclick="gotoLearn()">我的学习</a></li>
                    <li class="peraccout cur"><a>我的账户</a></li>
                   <li class="perorder" ><a href="MyOrder.aspx">我的订单</a></li>
                    <li class="perQuan"><a href="MyCoupon.aspx" class="marlt20">我的优惠券</a></li>
                   <li class="perset"><a href="PersonalSet.aspx">个人设置</a></li>
                   <li class="perstudy "><a href="StudyArchives.aspx">学习档案</a></li>
                   
                   
                </ul>
                <span class="lricon"></span>
            </div>
            <!--右栏-->
            <div class="perrgtbox">
                <div class="pertit">我的账户</div>
                <dl class="accinfo">
                    <dt>账户余额</dt>
                    <dd></dd>
                </dl>
                <div class="choinfo">收支记录<span class="marlt20">
                <select class="selist" onchange="currentTime=this.selectedIndex;getUserVirtualCoinOrderInfo();">
                <option value="0">全部记录</option>
                <option value="1">最近一周</option>
                <option value="2">最近两周</option>
                <option value="3">最近一月</option>
                <option value="4">最近两月</option>
                <option value="5">最近三月</option>
                </select></span><span class="fr marlt20">总计支出：<em class="redtxt" id="outMoney">0</em> K币</span><span class="fr">总计收入：<em class="gretxt" id="inMoney">0</em> K币</span></div>
                <div class="accout" >
                    <table cellpadding="0" cellspacing="0" class=" ">
                        <thead>
                            <tr>
                                <th width="15%" class="tLeft dNone">交易号</th>
                                <th width="25%" class="tLeft">名称</th>
                                <th width="20%" class="tLeft">成交时间</th>
                                <th width="10%" class="tRight">收支</th>
                                <th width="10%" class="tLeft">收支方式</th>
                            </tr>
                        </thead>
                        <tbody id="myOrder">
                            
                        </tbody>
                    </table>
                    <!--没有数据时 显示 -->
                    <div class="nodata dNone"></div>

                    <!--分页 -->
                    <div class="pagenation clearfloat">
                        <div class="pNum" id="pageSize"><select onchange="getUserVirtualCoinOrderInfo()"><option value="10">10条/页</option><option value="20">20条/页</option></select></div>
                        <div id="Pagination"></div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <div class="layer">
        <div class="layer-bg"></div>
        <!--弹框充值 -->
        <div class="div-table charge_box dNone">
            <div class="layertab"><span>k币充值</span><a class="cloesbtn clearVal"></a></div>
            <div class="layer-main">
                <dl class="laylist clearfloat">
                    <dt style="text-align:right;padding-right:14px">学习卡：</dt>
                    <dd><input type="text" id="card" maxlength="30"  placeholder="请输入您的学习卡卡号" onchange="$('#tipCard').hide()" /><p id="tipCard" class="error dNone">请输入您的学习卡卡号不对！</p></dd>
                    <dt style="text-align:right; padding-right:14px">密码：</dt>
                    <dd><input type="text" id="pwd" maxlength="20"  placeholder="请输入您的学习卡密码" onchange="$('#tipPwd').hide()" /><p id="tipPwd" class="error dNone">请输入您的学习卡密码不对！</p></dd>
                </dl>
                <div class="laybtn"><a class="laycancel clearVal">取消</a><a class="laycomfirm1 charge_result" onclick="chargeMoney()">充值</a></div>
            </div>
        </div>
        <!--充值结果 -->
        <div class="div-table charge_result_box dNone">
            <div class="layertab"><span>温馨提示</span><a class="cloesbtn"></a></div>
            <div class="layer-main">
                <h2>充值成功！本次共充值 <span class="gretxt">50000</span> k币</h2>
                <p id="cCard">学习卡卡号：205555856865dkfdlfwevweolmmdk</p>
                <p class="last">学习卡面值：500元</p>
                <div class="laybtn martop20"><a class="laycomfirm">确定</a></div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        var allCount = 0; //数量
        var currentTime = 0;
        var currentpageSize = 10; //当前分页大小
        var rate = 100;//支付比例
        $(function () {
            /*输入框相关*/
            $(".lgre_ipwrapper input").click(function () {
                $(this).parent().addClass("inting").find("input").prev().hide();
            });
            //获取比率
            $.ajax({
                type: "post",
                async: false,
                url: "../WebAjax/CourseInfo.aspx?" + Math.random(),
                data: { "fnName": "getRate" },
                success: function (d) {
                    if (d != "") {
                        rate = d;
                    }
                }
            });
            $(".clearVal").click(function () {
                //清空卡号密码输入框
                $("#card").val('');
                $("#pwd").val('');
            });
            initUserAccount();

        })
        

    </script>

        </div>
        <!-- 底部-->
        <div class="footer">
            <p>
                <a style="color: #959595" href="../other/about.aspx">关于我们</a> | <a style="color: #959595" href="../other/contactUs.aspx">联系我们</a>
                | <a style="color: #959595" href="../other/joinUs.aspx">加入我们</a> | <a style="color: #959595" href="../other/businessCooperation.aspx">商务合作</a> | <a style="color: #959595" href="../other/teacherCoorperation.aspx">教师合作</a>| <a style="color: #959595" href="http://www.wyk8.com:801/CardsQuery.aspx" target="_blank">充值查询</a>
            </p>
            <p style="color: #959595">
                南京易考无忧科技有限公司 版权所有 苏ICP备08005624号</p><p style="color: #959595"><img src="../images/ba.png" ><span style="margin-left:10px">苏公网安备 32010602010461号</span></p>
        </div>
        <div class="layer">
            <!--意见反馈-->
            <div class="div-table suggust_tipbox dNone">
                <div class="layertab">
                    <span class="">意见反馈</span> <a class="cloesbtn"></a>
                </div>
                <div class="layer-main">
                    <dl>
                        <dt>反馈内容<em class="import">*</em></dt>
                        <dd>
                            <textarea placeholder="请把您对于网站的建议或意见告诉我们吧！" id="suggests" maxlength="200" onkeyup="getNumberOfWords()"></textarea>
                            <p id="wordsNum">
                                0/200
                            </p>
                        </dd>
                        <dt>请输入您的邮箱 <em class="import">*</em></dt>
                        <dd>
                            <input type="text" placeholder="请输入您的邮箱号" id="Mail" /></dd>
                        <dt>请输入您的qq </dt>
                        <dd>
                            <input type="text" placeholder="请输入您的qq号码" id="QQ" /></dd>
                    </dl>
                    <div class="martop20">
                        <a class="gretxt" href="../other/Suggest.aspx">查看以往记录>> </a>
                    </div>
                    <div class="laybtn martop20">
                        <a class="laycancel" onclick='$("#Mail").val(""); $("#QQ").val("");$("#suggests").val("");'>取消</a><a class="laycomfirm1" onclick="submitSuggest()">提交</a>
                    </div>
                </div>
            </div>
            <!--错误信息弹出框 -->
            <div class="div-table common_tips dNone">
                <div class="layertab">
                    <span>温馨提示</span><a class="cloesbtn"></a>
                </div>
                <div class="layer-main clearfloat">
                    <p class="errtxt martop20">
                        输入错误
                    </p>
                    <div class="laybtn martop20">
                        <a class="laycomfirm">确定</a>
                    </div>
                </div>
            </div>

        </div>
        <!--侧边栏-->
		
		<ul class="slider_rbanner">

        <li class="kefu">
            <a href="http://q.url.cn/ABA4EU?_type=wpa&qidian=true" target="_blank">在线客服</a>
        </li>
        <li class="tel">
            <a>电话</a>
            <div class="dNone"><p>客服电话</p><p style="margin-top: 5px; font-weight:bold">400-600-2366</p></div>
        </li>
        <li class="app">
            <a>APP</a>
            <div class="dNone"><img src="../images/slider/app.jpg" alt="" /><p>扫码下载APP</p></div>
        </li>
        <li class="gongzhonghao">
            <a>公众号</a>
            <div class="dNone"><img src="../images/slider/无忧考吧.jpg" alt="" /><img src="../images/slider/会计网校.jpg" alt="" /><label>无忧考吧</label><label>会计网校</label><p>扫一扫，关注公众号</p></div>
        </li>
        <li class="weixin">
            <a>小程序</a>
            <div class="dNone"><img src="../images/slider/小程序.png" alt="" /><p>扫码使用小程序</p></div>
        </li>
        <!--<li class="suggust">
            <a>意见反馈</a>
        </li>-->
        <li class="top">
            <a>返回顶部</a>
        </li>
    </ul>

		
		
    </form>
    <script type="text/javascript">
        var isLogin = 0;
        $(function () {



            var source = request("s");
            if (source == undefined || source == "") {
                $(".header_nav").children().removeClass("cur");
                $("#d0").addClass("cur");
            }
            if (source == 1) {
                $(".header_nav").children().removeClass("cur");
                $("#d1").addClass("cur");
            }
            if (source == 2) {
                $(".header_nav").children().removeClass("cur");
                $("#d2").addClass("cur");
            }
            $(".allunread").click(function () {
                $(".unread").hide();
                $(".readed").addClass("unable");
            });
            $(".readed").click(function () {
                $(this).addClass("unable");
                $(this).parent().parent().find(".unread").hide();
            });
            $(".ndetail").click(function () {
                var cls = 'news_detbox';
                tipbox(1, cls);
            });
            document.onkeydown = function (e) {
                var ev = document.all ? window.event : e;
                if (ev.keyCode == 13) {
                    searchInfo(); //搜索信息
                }
            }
            /*悬浮购买*/
            $(window).scroll(function () {
                var theight = $(document).scrollTop();
                if (theight > 100) {
                    $(".fixed_buybox").removeClass("dNone");
                } else {
                    $(".fixed_buybox").addClass("dNone");
                }
            });
            /*意见反馈*/
            $(".suggust a").click(function () {
                //用户是否已登录
                $.ajax({
                    type: "get",
                    async: false,
                    url: "../WebAjax/UserInfo.aspx?" + Math.random(),
                    data: { "fnName": "isUserLogin" },
                    success: function (d) {
                        if (d != "") {
                            if (d == "-1") {
                                window.location.href = "../Login.aspx";
                                return;
                            }
                            else {
                                var cls = 'suggust_tipbox';
                                tipbox(1, cls);
                            }
                        }
                    }
                });

            });
            /*返回顶部*/
            $(".top a").click(function () {
                var sc = $(window).scrollTop();
                $('body,html').animate({ scrollTop: 0 }, 500);
            })
            $(".cloesbtn,.laycancel,.laycomfirm").click(function () {
                $(".layer").hide();
            })
            //用户是否已登录
            $.ajax({
                type: "get",
                async: false,
                url: "../WebAjax/UserInfo.aspx?" + Math.random(),
                data: { "fnName": "isUserLogin" },
                success: function (d) {
                    if (d != "") {
                        if (d == "-1") {
                            isLogin = 0;
                            $("#n").hide();
                            $(".ucenter").empty().append('<span class="nologin"><a href="../Login.aspx">登录</a><a href="../Regist.aspx">注册</a></span>');
                        }
                        else {
                            isLogin = 1;
                            //获取消息数量
                            $.ajax({
                                type: "post",
                                async: false,
                                url: "../WebAjax/UserInfo.aspx?" + Math.random(),
                                data: { "fnName": "getMessagesCount", "status": 1 },
                                success: function (d) {
                                    if (d != "") {
                                        $("#n").html(d);
                                        if (d == 0) {
                                            $("#n").hide();
                                        }
                                        else {
                                            $("#n").show();
                                        }
                                    }
                                }
                            });

                            //获取用户昵称和图片
                            $.ajax({
                                type: "post",
                                async: false,
                                url: "../WebAjax/UserInfo.aspx?" + Math.random(),
                                data: { "fnName": "getUserInfo" },
                                success: function (d) {
                                    if (d != "") {
                                        eval("UserObj=" + d.replace("\n", "<br/>"));
                                        var phone = "../images/usercent_user_icon.png";
                                        if (UserObj.Table[0].imgurl != "")
                                            phone = UserObj.Table[0].imgurl;
                                        $("#nickName").html('<em class="ellipsis">hi！' + UserObj.Table[0].UserName + '</em><img id="uImg" src="' + phone + '">');
                                    }
                                }
                            });
                        }
                    }
                }
            });
        })
        var searchInfo = function () {
            if (window.location.href.indexOf("findMain.aspx") > 0 || window.location.href.indexOf("SearchInfo.aspx") > 0)//当前为套餐页面则进行套餐内容搜索
            {
                var condition = $("#condition").val();
                condition = decodeURIComponent(condition);
                window.location.href = "../search/SearchInfo.aspx?condition=" + condition;
            }
        }

        var submitSuggest = function () {
            currenttext = $.trim(currenttext);
            if (currenttext == "" || currenttext == undefined) {
                wyAlert("请输入意见内容");
                return;
            }
            //获取邮箱号
            var txtMail = $("#Mail").val();
            //验证邮箱号的正确性
            if ($.trim(txtMail) == "" || txtMail == "请输入您的邮箱号") {
                wyAlert('请输入邮箱！');
                return;
            }
            if (!String.ValidateFunc.Validate(txtMail, String.ValidateFunc.Mail)) {
                wyAlert('邮箱格式不正确！');
                return;
            }
            //获取QQ号
            var qq = $("#QQ").val();
            if (qq != "") {
                if (!String.ValidateFunc.Validate(qq, String.ValidateFunc.QQ)) {
                    wyAlert('QQ号格式不正确！');
                    return;
                }
            }
            $.ajax({
                type: "post",
                async: false,
                url: "../WebAjax/UserInfo.aspx?" + Math.random(),
                data: { "fnName": "saveSuggest", "content": currenttext, "mail": txtMail, "qq": qq },
                success: function (d) {
                    if (d == 0) {
                        wyAlert("保存成功");
                        $(".layer").hide();
                        $("#Mail").val("");
                        $("#QQ").val("");
                        $("#suggests").val("");
                    }
                    else {
                        if (d == -1) {
                            window.location.href = "../Login.aspx";
                            return;
                        }
                        wyAlert("保存失败");
                    }
                }
            });

        }
        var currenttext = "";
        var getNumberOfWords = function () {
            var text = $("#suggests").val();
            //判断特殊字符
            //            if (text.indexOf('\'') != -1 || text.indexOf('\"') != -1 ) {
            //                alert("请勿使用 ' \"等特殊字符！");
            //            }

            var len = text.length;
            if (len > 200) {
                $("#suggests").text(currenttext);
            }
            else {
                $("#wordsNum").text(len + "/200");
                currenttext = text;
            }
        }
        var changeSelect = function (obj) {
            $(obj).parent().children().removeClass("cur");
            $(obj).addClass("cur");
        }

        var loginOut = function () {
            $.ajax({
                type: "get",
                async: false,
                url: "../WebAjax/UserInfo.aspx?" + Math.random(),
                data: { "fnName": "loginOut" },
                success: function (d) {

                }
            });
        }
        var gotoMessage = function () {
            if (isLogin == 0) {
                window.location.href = "../Login.aspx";
            }
            else {
                window.location.href = "../mesage/message.aspx";
            }
        }

    </script>
    
 <script type="text/javascript">
     $(function () {
         $(".header_nav").children('a').removeClass("cur");
     });
 </script>

</body>
</html>
