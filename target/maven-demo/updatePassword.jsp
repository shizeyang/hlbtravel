<%--
  Created by IntelliJ IDEA.
  User: 19245
  Date: 2019/11/9
  Time: 10:00
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>

<div class="pwdForm">
    <div class="class-pwd-title">修改密码</div>
    <div class="class-pwd-table">
        <div class="class-input-line">
            <label>输入密码：</label>
            <input type="password" class="oldPassWord"
                   data-options="required:true,missingMessage:'不能为空'" maxlength="32" minlength="8">
        </div>
        <div class="class-input-line">
            <label>确认密码：</label>
            <input type="password" class="newPassWord"
                   data-options="required:true,missingMessage:'不能为空'" maxlength="32" minlength="8">
        </div>

        <div class="surePassWordBtn" id="password-button">
            <a href="#" iconCls="icon-vfp-save" id="sure-button" class="class-btn class-btn-sure">确定</a>
            <a href="#" iconCls="icon-vfp-save" id="id-return-button" class="class-btn class-btn-return">返回</a>
        </div>
    </div>


    <script src="js/jquery-3.3.1.js"></script>
    <script>
        $(function () {
            $("a[id=sure-button]").click(function () {
                var pwd1 = $("input[class=oldPassWord]").val();
                var pwd2 = $("input[class=newPassWord]").val();
                if (pwd1 == "" || pwd2 == ""){
                    alert("密码不能为空！");
                } else if (pwd2 != pwd1){
                    alert("两次密码不一致，请重新输入！")
                } else {
                    $.getJSON("user/updatepassword",{"password":pwd1,"id":${sessionScope.user.id}},function (data) {
                        if (data==1){
                            alert("修改成功");
                        }
                    })


                }


            })

        })


    </script>
</body>
</html>
