<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2019/10/29
  Time: 15:57
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>路线详情</title>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/common.css">
    <link rel="stylesheet" type="text/css" href="css/route-detail.css">
    <link rel="stylesheet" type="text/css" href="css/button.css">






</head>

<body>

<%
    int id = Integer.parseInt(request.getParameter("id"));
    /*int uid = Integer.parseInt(String.valueOf(session.getAttribute("uid")));*/

%>
<!--引入头部-->
<div id="header"></div>
<!-- 详情 start -->
<div class="wrap">
    <div class="bread_box">
        <a href="/">路线详情-</a>
        <span id="travelluxian"></span>

        <a href="#"></a>
    </div>
    <div class="prosum_box">
        <dl class="prosum_left">
            <dt>
                <img alt="" class="big_img" src="http://img.jinmalvyou.com/img_size4/201703/m40eeb6bdb49d8f74b0987d0c6a17aac4b.jpg">
            </dt>
            <dd>
                <a class="up_img up_img_disable"></a>
                <a title="" class="little_img" data-bigpic="http://img.jinmalvyou.com/original_img/201703/bee1713c251ebadc0d30774d8d7050e9.jpg">
                    <img src="http://img.jinmalvyou.com/original_img/201703/bee1713c251ebadc0d30774d8d7050e9.jpg">
                </a>
                <a title="" class="little_img cur_img" data-bigpic="http://m.tuniucdn.com/fb2/t1/G4/M00/99/E8/Cii_J10a-LOIbTzeAAaCr-6msWcAAHj6ALOgmkABoLH876_w500_h280_c1_t0.jpg">
                    <img src="http://m.tuniucdn.com/fb2/t1/G4/M00/99/E8/Cii_J10a-LOIbTzeAAaCr-6msWcAAHj6ALOgmkABoLH876_w500_h280_c1_t0.jpg">
                </a>
                <a title="" class="little_img" data-bigpic="http://img.jinmalvyou.com/img_size4/201703/m4a776a65297e270884447e47bc5fec9be.jpg">
                    <img src="http://img.jinmalvyou.com/img_size4/201703/m4a776a65297e270884447e47bc5fec9be.jpg">
                </a>
                <a title="" class="little_img" data-bigpic="http://img.jinmalvyou.com/20181226/goods_thumb_20714_600_350.jpeg">
                    <img src="http://img.jinmalvyou.com/20181226/goods_thumb_20714_600_350.jpeg">
                </a>
                <a title="" class="little_img" data-bigpic="http://img.jinmalvyou.com/20190517/7832e18c028545b2143384fe7307e8fc.jpg" style="display:none;">
                    <img src="http://img.jinmalvyou.com/20190517/7832e18c028545b2143384fe7307e8fc.jpg">
                </a>
                <a title="" class="little_img" data-bigpic="http://img.jinmalvyou.com/20190131/goods_thumb_21082_600_350.jpeg" style="display:none;">
                    <img src="http://img.jinmalvyou.com/20190131/goods_thumb_21082_600_350.jpeg">
                </a>
                <a title="" class="little_img" data-bigpic="http://img.jinmalvyou.com/20190125/goods_thumb_21055_600_350.jpeg" style="display:none;">
                    <img src="http://img.jinmalvyou.com/20190125/goods_thumb_21055_600_350.jpeg">
                </a>
                <a title="" class="little_img" data-bigpic="http://img.jinmalvyou.com/img_size4/201703/m40eeb6bdb49d8f74b0987d0c6a17aac4b.jpg" style="display:none;">
                    <img src="http://img.jinmalvyou.com/img_size4/201703/m40eeb6bdb49d8f74b0987d0c6a17aac4b.jpg">
                </a>
                <a title="" class="little_img" data-bigpic="http://img.jinmalvyou.com/img_size4/201703/m475fee8c2997890e4d304efae6a4fe54e.jpg" style="display:none;">
                    <img src="http://img.jinmalvyou.com/img_size4/201703/m475fee8c2997890e4d304efae6a4fe54e.jpg">
                </a>
                <a class="down_img down_img_disable" style="margin-bottom: 0;"></a>
            </dd>
        </dl>
        <div class="prosum_right">
            <p class="pros_title"></p>
            <p class="hot"></p>
            <div class="pros_other">
                <p>经营商家  ：胡萝卜国旅</p>
                <p>咨询电话 : 400-823-823</p>
                <p>地址 ： KGC胡萝卜程序员</p>
            </div>
            <div class="pros_price">
                <p class="price"><strong>￥</strong></p>
                <p class="collect">
                    <a class="btn" id="btn_favorite"><i class="glyphicon glyphicon-heart-empty"></i>点击收藏</a>
                    <a class="btn" id="btn_orders"><i class="glyphicon glyphicon-heart-empty"></i>加入订单</a>

                </p>
            </div>
        </div>
    </div>

<div id="J_orderBox_toolbar" class="res_order res_order_fixed">
    <div class="res_order_btn"><a href="javascript:;" rel="nofollow" class="res_btn " id="purchase">开始预订</a>
        <div class="res_price" style="position:relative"><strong>总价：￥</strong><span class="res_fee"></span>
        </div>
    </div>
    <ul class="res_chose">
        <li><label class="lab">出发时间：</label>
            <div class="input_box">
                <input type="date" id="J_DepartDate" name="J_DepartDate" >

            </div>
        </li>
        <li><label class="lab">线路</label>
            <div class="input_box input_per"  >
                <select name="" style="">
                    <option value="0">线路A</option>
                    <option value="1">线路B</option>
                    <option value="3">线路C</option>
                </select>
            </div></li>
        <li><label class="lab">人数：</label>
            <div class="input_box input_per" >
                <select name="person" style="" >
                    <option value="1">1人</option>
                    <option value="2">2人</option>
                    <option value="3">3人</option>
                    <option value="4">4人</option>
                    <option value="5">5人</option>
                </select>
            </div>
        </li>
        <%--<li><label class="lab">儿童</label>
            <div class="input_box input_per"  >
                <select name="" >
                    <option value="0">1人</option>
                    <option value="1">2人</option>
                </select>
            </div>
            <em class="kid_tip"><span class="base_tip">2-12周岁（不含）</span></em>
        </li>--%>
    </ul>
</div>
    <div class="you_need_konw">
        <span>旅游须知</span>
        <div class="notice">
            <p>1、旅行社已投保旅行社责任险。建议游客购买旅游意外保险 <br>

            <p>2、旅游者参加打猎、潜水、海边游泳、漂流、滑水、滑雪、滑草、蹦极、跳伞、滑翔、乘热气球、骑马、赛车、攀岩、水疗、水上飞机等属于高风险性游乐项目的，敬请旅游者务必在参加前充分了解项目的安全须知并确保身体状况能适应此类活动；如旅游者不具备较好的身体条件及技能，可能会造成身体伤害。</p>

            <p>3、参加出海活动时，请务必穿着救生设备。参加水上活动应注意自己的身体状况，有心脏病、冠心病、高血压、感冒、发烧和饮酒及餐后不可以参加水上活动及潜水。在海里活动时，严禁触摸海洋中各种鱼类，水母，海胆，珊瑚等海洋生物，避免被其蛰伤。老人和小孩必须有成年人陪同才能参加合适的水上活动。在海边游玩时，注意保管好随身携带的贵重物品。</p>

            <p>4、根据中国海关总署的规定，旅客在境外购买的物品，在进入中国海关时可能需要征收关税。详细内容见<a href="http://www.gov.cn/gongbao/content/2010/content_1765293.htm">《中华人民共和国海关总署公告2010年第54号文件》</a>。</p>

            <p>5、建议出发时行李托运，贵重物品、常用物品、常用药品、御寒衣物等请随身携带，尽量不要托运。行李延误属于不可抗力因素，我司将全力协助客人跟进后续工作，但我司对此不承担任何责任。</p>

            <p>6、为了您人身、财产的安全，请您避免在公开场合暴露贵重物品及大量现金。上街时需时刻看管好首饰、相机等随身物品。 <br>

            <p>7、请您严格遵守境外旅游目的地有关国家法律法规，切勿从事象牙等濒危野生动植物及其制品交易或携带相关物品，避免因触犯法律损害自身利益。</p>
            <p >8、携程从出行常识、旅游活动（风险性项目）和特殊人群三方面为您提供旅游安全指南，请您仔细阅读<a href="https://vacations.ctrip.com/notes/4939.html" style="color:red">安全指南及警示</a>。</p>

            <p>9、根据发布的<a href="https://vacations.ctrip.com/notes/4939.html" style="color: red">《中华人民共和国禁止携带、邮寄进境的动植物及其产品和其他检疫物名录》</a>，将燕窝、新鲜水果、蔬菜、动物源性、转基因生物材料等列入严禁携带或邮寄进境项目，敬请知晓。</p>

            <p>10、本产品支持信用卡、网银/第三方、支付宝、微信、现金余额、拿去花支付，具体以支付页为准。</p>

            <p>下单后胡萝卜旅游会短信通知您材料邮寄的具体地址及收件人，同时南京地区部分胡萝卜旅游提供免费代收服务：门店详情;<br>
                1、如需撤签，提供护照原件及亲笔签名的撤签说明复印件，撤签说明需包含申请人姓名、护照号码、撤签原因、办签时预订的出行日期；<br>
                2、如您在出行之前出行时间、行程、机票、酒店信息有变更，请及时联系携程重新提供相关变更信息（更改后的行程也需在我司预订），以免影响您正常出行；若前往冲绳，则无法变更为其他城市；<br>
                3、为了方便领馆审核您的护照和贴签证页，在您递交材料前请将护照保护套取下并自行保管，以避免在领馆审核时有所遗失，敬请配合；</p>
        </div>
    </div>
</div>
<!-- 详情 end -->

<!--引入头部-->
<div id="footer"></div>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="js/jquery-3.3.1.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/bootstrap.min.js"></script>
<!--导入布局js，共享header和footer-->
<script type="text/javascript" src="js/include.js"></script>
<script>
    $(document).ready(function() {

    $.getJSON("travel/details",{"id":<%=id%>},function (data) {
        var id = <%=id%>;
        var details = data.details;
        var discount = data.discount;
        var price = data.price;
        var departure = data.departure;
        var destination = data.destination;
        var picture = data.picture;
        var brief = data.brief;
        var fenlei = data.fenlei;
        var uid = ${sessionScope.user.id};
        $("span[class=res_fee]").append(price);
        /*var person = $("select[name=person] option").val();

        var totalprice = price*person;*/
        /*$("select[name=person] option").onchange(function () {
            $("span[class=res_fee]").append(totalprice);
        })*/

        $("select[name=person]").change(function () {
            var person = $("select[name=person]").val();

            var totalprice = price*person;
            $("span[class=res_fee]").empty();
            $("span[class=res_fee]").append(totalprice);

        })

      /*  $("span[class=res_fee]").append(totalprice);
*/



        if (data!=null){
            $("span[id=travelluxian]").append(data.brief);
           $("P[class=pros_title]").append(data.details);
           $("p[class=hot]").append(data.discount);
           $("p[class=price] strong").append(data.price);

        }
        $("a[id=btn_favorite]").click(function () {
            ;
            $.getJSON("travel/favorite",{"id":id,"details":details,"discount":discount,"price":price,"departure":departure,"destination":destination,"picture":picture,"brief":brief,"fenlei":fenlei,"uid":uid},function (data1) {
                if (data1==1){
                    alert("添加收藏成功！")
                    $("a[id=btn_favorite]").attr("disabled",true);
                    $("a[id=btn_favorite]").remove("href");


                }else {
                    alert("已经在个人收藏中，无法重复添加！")
                }
            })
        })
        $("a[id=btn_orders]").click(function () {
            $.getJSON("orders/add",{"details":details,"price":price,"picture":picture,"uid":uid},function (data) {
                if (data == 1){
                    alert("增加成功");
                }
            })
        })
        
        $("a[id=purchase]").click(function () {

            var sumprice = $("span[class=res_fee]").html();
            var sumperson = $("select[name=person]").val();
            var detail = $("P[class=pros_title]").html();
            var time = $("input[name=J_DepartDate]").val();
            window.location.href = "purchase.jsp?price="+sumprice+"&person="+sumperson+"&detail="+detail+"&time="+time;



        })


    })

















        //焦点图效果
        //点击图片切换图片
        $('.little_img').on('mousemove', function() {
            $('.little_img').removeClass('cur_img');
            var big_pic = $(this).data('bigpic');
            $('.big_img').attr('src', big_pic);
            $(this).addClass('cur_img');
        });
        //上下切换
        var picindex = 0;
        var nextindex = 4;
        $('.down_img').on('click',function(){
            var num = $('.little_img').length;
            if((nextindex + 1) <= num){
                $('.little_img:eq('+picindex+')').hide();
                $('.little_img:eq('+nextindex+')').show();
                picindex = picindex + 1;
                nextindex = nextindex + 1;
            }
        });
        $('.up_img').on('click',function(){
            var num = $('.little_img').length;
            if(picindex > 0){
                $('.little_img:eq('+(nextindex-1)+')').hide();
                $('.little_img:eq('+(picindex-1)+')').show();
                picindex = picindex - 1;
                nextindex = nextindex - 1;
            }
        });
        //自动播放
        // var timer = setInterval("auto_play()", 5000);
    });

    //自动轮播方法
    function auto_play() {
        var cur_index = $('.prosum_left dd').find('a.cur_img').index();
        cur_index = cur_index - 1;
        var num = $('.little_img').length;
        var max_index = 3;
        if ((num - 1) < 3) {
            max_index = num - 1;
        }
        if (cur_index < max_index) {
            var next_index = cur_index + 1;
            var big_pic = $('.little_img:eq(' + next_index + ')').data('bigpic');
            $('.little_img').removeClass('cur_img');
            $('.little_img:eq(' + next_index + ')').addClass('cur_img');
            $('.big_img').attr('src', big_pic);
        } else {
            var big_pic = $('.little_img:eq(0)').data('bigpic');
            $('.little_img').removeClass('cur_img');
            $('.little_img:eq(0)').addClass('cur_img');
            $('.big_img').attr('src', big_pic);
        }
    }









</script>
</body>

</html>